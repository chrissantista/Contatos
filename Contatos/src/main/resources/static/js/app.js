/**
 * 
 */
//var app = angular.module('myApp', ['base64']);
var app = angular.module('myApp', []);

app.controller('myCtrl', function($scope, $http) {
	
	$scope.contatos = [];
	$scope.contato = {};
	$scope.formContato = false;

	
	listaContatos = function() {
		$http.get("http://127.0.0.1:8080/contatos")
		.then(function(response) {
			$scope.contatos = response.data;
		}, function(response) {
			console.log(response.data);
			console.log(response.status);
		})
	}
	
	$scope.salvaContato = function() {
		$http.post("http://127.0.0.1:8080/contatos", $scope.contato)
		.then(function(response) {
			listaContatos();
			$scope.contato = {};
		}, function(response) {
			console.log(response.data);
			console.log(response.status);
		})
		$scope.formContato = false;
	}
	
	$scope.removeContato = function(id, index) {
		$http.delete("http://127.0.0.1:8080/contatos/" + id)
		.then(function(response) {
			$scope.contatos.splice(index, 1);
		}, function(response) {
			console.log(response.data);
			console.log(response.status);
		})
	}
	
	$scope.novoContato = function() {
		$scope.contato = {};
		$scope.formContato = true;
	}
	
	$scope.alteraContato = function(contato) {
		$scope.formContato = true;
		$scope.contato = angular.copy(contato);
	}
	
	$scope.cancelaContato = function() {
		$scope.contato = {};
		$scope.formContato = false;
	}
	
	listaContatos();
	
})