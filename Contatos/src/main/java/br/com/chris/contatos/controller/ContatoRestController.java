package br.com.chris.contatos.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.chris.contatos.model.Contato;
import br.com.chris.contatos.model.ContatoDao;

@RestController
@RequestMapping("/contatos")
public class ContatoRestController {

	@Autowired
	private ContatoDao dao;
	
	@GetMapping()
	public ResponseEntity<List<Contato>> getAllContatos() {
		return new ResponseEntity<>(dao.findAll(), HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<Contato> getContato(@PathVariable Long id) {
		return new ResponseEntity<>(dao.findOne(id), HttpStatus.OK);
	}
	
	@GetMapping(params = {"nome"})
	public ResponseEntity<List<Contato>> getContatosByNome(
			@RequestParam(value="nome") String nome) {
		return new ResponseEntity<>(dao.findByNomeStartingWith(nome), HttpStatus.OK);
	}
	
	@PostMapping()
	public ResponseEntity<Contato> addContato(@RequestBody Contato contato) {
		return new ResponseEntity<>(dao.save(contato), HttpStatus.OK);
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<Contato> updateContato(@RequestBody Contato contato, @PathVariable Long id) {
		Contato contatoSave = new Contato();
		contatoSave.setId(id);
		contatoSave.setNome(contato.getNome());
		contatoSave.setEmail(contato.getEmail());
		contatoSave.setTel(contato.getTel());
		return new ResponseEntity<>(dao.save(contatoSave), HttpStatus.OK);
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<?> deleteContato(@PathVariable Long id) {
		dao.delete(dao.findOne(id));
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
}
