package br.com.chris.contatos.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter{
	
	@Autowired
	private DataSource dataSource;

	@Override
	public void configure(AuthenticationManagerBuilder builder) 
			throws Exception{
		builder
			.jdbcAuthentication()
				.dataSource(dataSource)
				.usersByUsernameQuery("select username, password, enabled from usuario where username = ?")
				.authoritiesByUsernameQuery("select username, role from usuario where username = ?");
		
			/*.inMemoryAuthentication()
			.withUser("chris").password("123").roles("USER")
			.and()
			.withUser("admin").password("321").roles("ADMIN");*/
	}
	
	@Override
	public void configure(HttpSecurity http) throws Exception {
		http
			.formLogin().loginPage("/login").permitAll()
			.failureUrl("/login?erro=true")
			.and()
			.logout().logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
				.logoutSuccessUrl("/login")
			.and()
			.authorizeRequests()
				.antMatchers(HttpMethod.POST, "/contatos").hasAuthority("ADMIN")
				.antMatchers(HttpMethod.PUT, "/contatos/*").hasAuthority("ADMIN")
				.antMatchers(HttpMethod.DELETE, "/contatos/*").hasAuthority("ADMIN")
				.anyRequest().authenticated()
			.and()
			.csrf().disable();
			
	}
}
