package br.com.chris.contatos.controller;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.chris.contatos.model.Usuario;

@Controller
public class HomeController {

	@RequestMapping("/")
	public String inicio() {
		return "index";
	}
	
	@RequestMapping("login")
	public String login() {
		return "login";
	}
	
	@RequestMapping("/sessao")
	public String sessao(Model model) {
		UserDetails userDetails =
				 (UserDetails)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		model.addAttribute("usuario", userDetails.getUsername());
		model.addAttribute("autoridades", userDetails.getAuthorities());
		return "sessao";
	}
}
