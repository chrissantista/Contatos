package br.com.chris.contatos.model;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface ContatoDao extends JpaRepository<Contato, Long> {
	
	List<Contato> findByNomeStartingWith(String nome);
}
